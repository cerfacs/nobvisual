# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).


### [0.3.2] 2021/12/16

### Added
* `short_text` argument to packing circle


## [0.3.1] 2021/10/27

### Changed
* name font for non-leaf nodes
* right-click behavior in leaves (zoom to parents) and in already-focused circle
* `is_leaf` as method instead of property


### Fixed
* name appearance for long strings



## [0.3.0] 2021/10/21


### Added

- zoom behavior
- `max_recursion` for `tree`


### Changed

- fully refactored internal workings (mostly OOP approach now)
- under focus text follows mouse motion
- moved several functions to `utils`


## [0.2.0] end of jan 2021

### Added

- colorscale option in tkinter_circlify



### Fixed

- colorshading was not working for lighter tones (Shade>0). Now fixed.



## [0.1.0 ] jan 2021

### Added

- CLI centered on `>nobvisual`
- `tree` to show a folder nested structure
- `treefile` to show a serialization file nested structure json/yaml/nml
- `cmpfile` to compare two serialization files
- `tkcirclify()` the base function showing a circlify object in tkinter. 

