import os

from click.testing import CliRunner

from nobvisual import cli


def test_tree():
    runner = CliRunner()
    result = runner.invoke(cli.tree, ['--max-recursion', 1, '--debug'])

    assert result.exit_code == 0


def test_treefile(datadir):
    filename = os.path.join(datadir, 'treefile.yaml')

    runner = CliRunner()
    result = runner.invoke(cli.treefile, [filename, '--debug'])

    assert result.exit_code == 0


def test_cmpfile(datadir):
    filename_left = os.path.join(datadir, 'file_left.yaml')
    filename_right = os.path.join(datadir, 'file_right.yaml')

    runner = CliRunner()
    result = runner.invoke(cli.cmpfile,
                           [filename_left, filename_right, '--debug'])

    assert result.exit_code == 0
