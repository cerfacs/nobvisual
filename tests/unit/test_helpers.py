
from nobvisual.helpers import from_nested_struct_to_nobvisual


def test_from_nested_struct_to_nobvisual():

    nstruct = [{'id': 0, 'datum': 2.0, 'name': 'root',
                'children': [
                    {'id': 1, 'datum': 1.0, 'short_name': '1',
                     'children': [
                         {'id': 2, 'datum': 1.0}]
                     },
                    {'id': 3, 'datum': 1.0, 'text': 'text-3',
                     'children': [
                         {'id': 4, 'datum': 1.0, 'color': 'blue'}]
                     },
                ], }]

    circles = from_nested_struct_to_nobvisual(nstruct)

    assert len(circles) == 5

    assert circles[0].name == 'root'
    assert len(circles[0].children) == 2
    assert circles[0].level == 1

    assert circles[1].short_name == '1'
    assert circles[1].level == 2
    assert circles[2].text == 'text-3'
    assert circles[1].is_leaf() is False

    assert circles[3].is_leaf() is True
    assert circles[3].level == 3
    assert circles[4].color == 'blue'
