
from nobvisual.nob2nstruct import build_nstruct
from nobvisual.nob2nstruct import TYPE2COLOR
from nobvisual.nob2nstruct import UnknownType


def test_nob2nstruct():

    class FakeType:
        pass

    init_dict = {'a': {'a1': False, 'a2': True},
                 'b': 1, 'c': 1.0, 'd': 'string', 'e': None, 'f': FakeType()}
    nstruct = build_nstruct(init_dict)

    assert len(nstruct) == 1

    children = nstruct[0]['children']
    assert children[1]['color'] == TYPE2COLOR[int]
    assert children[2]['color'] == TYPE2COLOR[float]
    assert children[3]['color'] == TYPE2COLOR[str]
    assert children[4]['color'] == TYPE2COLOR[type(None)]
    assert children[5]['color'] == TYPE2COLOR[UnknownType]

    children = children[0]['children']
    assert children[0]['color'] == TYPE2COLOR[bool]
    assert children[1]['color'] == TYPE2COLOR[bool]
