"""Module to ease the colorization of circles"""

from tol_colors import tol_cmap
from math import log10
import fnmatch
import matplotlib


EPS = 1.0e-10
BIG= 1.0e+10

def reset_colors(nstruc:list, default=None)-> list:
    """[RECURSIVE, INPLACE] Reset all colors to default color, which can be overriden"""
    for child in nstruc:
        try:
            child["color"]=default
            reset_colors(child["children"],default=default)
        except KeyError:
            pass
        
        
def color_by_name_pattern(nstruc:list, patterns:list)-> list:
    """[RECURSIVE, INPLACE] Change colors according to a list of patterns
    
    typically, a list of patterns looks like:

    [
       ("*phasefield*", "green"),
       ("*moment*", "red"),
       ("*__init__*", "yellow"),
    ]
    """
    
    for child in nstruc:
        if "name" in child:
            for pattern,color in patterns:
                if fnmatch.filter([child["name"]], pattern):
                    child["color"]=color
        if "children" in child:
            color_by_name_pattern(child["children"],patterns)


def color_by_value(nstruc:list, variable:str, invert:bool=False, logarithmic:bool=False, tolcmap='YlOrBr', min_val:float=None, max_val:float=None, leg_levels:int=6)-> list:
    """[RECURSIVE, INPLACE] Reset all colors to default color, which can be overriden"""
    
    cmap = tol_cmap(tolcmap)

    def _read_val(data):
        out = None
        if variable in data:
            out = data[variable]
            if logarithmic:
                out = log10(out)
        return out


    ## Find min value
    if min_val is None:
        def _miner(data:list, min_val=BIG):
            out = min_val
            for item in data:
                try:
                    out = min(_read_val(item),out )
                except TypeError:
                    pass
                if "children" in item:
                    out = min(_miner(item["children"], min_val=out), out)
            return out
        min_val = _miner(nstruc)
    else:
        if logarithmic:
            min_val=log10(min_val)
    
    # Find max value
    if max_val is None:
        def _maxer(data:list, max_val=-BIG):
            out = max_val
            for item in data:
                try:
                    out = max(_read_val(item),out )
                except TypeError:
                    pass
                if "children" in item:
                    out = max(_maxer(item["children"], max_val=out), out)
            return out
        max_val = _maxer(nstruc)
    else:
        if logarithmic:
            max_val=log10(max_val)
    

    def _color_child(child:dict):
        """Assign color to nodes"""
        if variable not in child:
            child["color"]=None
        else:
            val = child[variable]
            if logarithmic:
                val = log10(val)
            try:    
                val = (_read_val(child)-min_val)/(max_val-min_val+EPS)
            except TypeError:
                raise RuntimeError(f"Field {variable} not found")
            val = min(max(val,0.), 1.)
            if invert:
                val=1.-val
            child["color"]=matplotlib.colors.to_hex(cmap(val))
        if "children" in child:
            for granson in child["children"]:
                _color_child(granson)
    for child in nstruc:
        _color_child(child)

    #Build legend
    legend=[]
    for lvl in range(leg_levels):
        ratio = 1-lvl /(leg_levels-1)
        val = min_val+ratio*(max_val-min_val)
        if logarithmic:
            val=10**val
        str = f"{val:.2f}"

        if len(str)>4:
            str = str.split(".")[0]
        if invert:
            ratio=1.-ratio
        
        legend.append((str, matplotlib.colors.to_hex(cmap(ratio)) ))
    return legend


